<# 
########################################################################
#Created by: A.G.
#Purpose: Office 365 calendar permissions
#Date: 25/04/2024
#version: 1.0.20
########################################################################
#>
check_if_user_admin
Set-ExecutionPolicy Unrestricted -Force
$ErrorActionPreference = 'SilentlyContinue'

Remove-Item -Path .\languageHeb.txt -Force
Remove-Item -Path .\pic.jpg -Force
Invoke-WebRequest -Uri https://gitlab.com/Abebegebray/office365_calendar-permision/-/raw/main/languageHeb.txt -OutFile .\languageHeb.txt
Invoke-WebRequest -Uri https://gitlab.com/Abebegebray/office365_calendar-permision/-/raw/main/pic.jpg -OutFile .\pic.jpg
check_Module_ExchangeOnlineManagement
cls
Write-Host `n"~~ Check out " -NoNewline -ForegroundColor white; Write-Host "https://gitlab.com/Abebegebray" -ForegroundColor Yellow -NoNewline; Write-Host " to get access to scripts. ~~" -ForegroundColor white `n`n
`n`n
Write-Host Connecting to Exchange Online...
`n`n

Connect-ExchangeOnline

Add-Type -assembly System.Windows.Forms
Function Prompt(){}
Clear-Host
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[reflection.assembly]::loadwithpartialname("System.Windows.Forms") | Out-Null
[System.Windows.Forms.Application]::EnableVisualStyles()

$CalendarPermissions                            = New-Object system.Windows.Forms.Form
$CalendarPermissions.ClientSize                 = New-Object System.Drawing.Point(860,520)
$CalendarPermissions.text                       = "Office 365 calendar permissions                                           Hello, $env:username! "
$CalendarPermissions.TopMost                    = $false
$CalendarPermissions.FormBorderStyle = "FixedDialog"
$CalendarPermissions.backcolor = "#4fc3f7";$kCloseuser.forecolor = ""
$CalendarPermissions.StartPosition = "CenterScreen"

$FormTabControl = New-object System.Windows.Forms.TabControl
$FormTabControl.Size = "858,540"
$FormTabControl.Location = "0,0"
$FormTabControl.Font = [System.Drawing.Font]::new("Arial", 13, [System.Drawing.FontStyle]::Regular)
 
$Tab1 = New-object System.Windows.Forms.Tabpage
$Tab1.DataBindings.DefaultDataSourceUpdateMode = 0
#$Tab1.UseVisualStyleBackColor = $True
$Tab1.Name = "TAB1"
$Tab1.Text = "Calendar permissions"
$Tab1.Font = [System.Drawing.Font]::new("Arial", 9, [System.Drawing.FontStyle]::Regular)
$Tab1.backcolor = "#4fc3f7"

$Tab2 = New-object System.Windows.Forms.Tabpage
$Tab2.DataBindings.DefaultDataSourceUpdateMode = 0
$Tab2.UseVisualStyleBackColor = $True
$Tab2.Name = "TAB2"
$Tab2.Text = "Coming soon"
$Tab2.Font = [System.Drawing.Font]::new("Arial", 9, [System.Drawing.FontStyle]::Regular)

$CalendarPermissions.Controls.Add($FormTabControl)
$FormTabControl.Controls.Add($Tab1)
$FormTabControl.Controls.Add($Tab2)
 
 $objImage = [system.drawing.image]::FromFile(".\pic.jpg")
$Tab2.BackgroundImage = $objImage
#$Tab2.BackgroundImageLayout = "None"

# Time display label
$timeDisplay = New-Object System.Windows.Forms.Label
$timeDisplay.Location = New-Object System.Drawing.Point(725, 465)
$timeDisplay.Size = New-Object System.Drawing.Size(200, 20)
$Tab1.Controls.Add($timeDisplay)

# Update time every second
$timer = New-Object System.Windows.Forms.Timer
$timer.Interval = 1000
$timer.Add_Tick({
    $timeDisplay.Text = (Get-Date).ToString("yyyy-MM-dd HH:mm:ss")
})
$timer.Start()

$Checkinternet                        = New-Object system.Windows.Forms.Label
$Checkinternet.multiline              = $false
$Checkinternet.Size                   = New-Object System.Drawing.Size(200, 14)
$Checkinternet.location               = New-Object System.Drawing.Point(660,2)
$Checkinternet.Font                   = New-Object System.Drawing.Font('Microsoft Sans Serif',8)

$pleasewait                        = New-Object system.Windows.Forms.Label
$pleasewait.multiline              = $false
$pleasewait.width                  = 150
$pleasewait.height                 = 30
$pleasewait.location               = New-Object System.Drawing.Point(300,160)
$pleasewait.location               = New-Object System.Drawing.Point(300,160)
$pleasewait.Font                   = New-Object System.Drawing.Font('Microsoft Sans Serif',15)
$pleasewait.BackColor              = "Transparent"

$Emailadmin                        = New-Object system.Windows.Forms.TextBox
$Emailadmin.multiline              = $false
$Emailadmin.width                  = 250
$Emailadmin.height                 = 20
$Emailadmin.location               = New-Object System.Drawing.Point(20,32)
$Emailadmin.Font                   = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$Bottonconnect                         = New-Object system.Windows.Forms.Button
$Bottonconnect.text                    = "Connect"
$Bottonconnect.width                   = 85
$Bottonconnect.height                  = 30
$Bottonconnect.location                = New-Object System.Drawing.Point(300,32)
$Bottonconnect.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
$Bottonconnect.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
$Bottonconnect.BackColor                  = [System.Drawing.ColorTranslator]::FromHtml("#417505")

$Labelconnected                         = New-Object system.Windows.Forms.Label
$Labelconnected.Text = "Connected to:"
$Labelconnected.width                   = 400
$Labelconnected.height                  = 20
$Labelconnected.location                = New-Object System.Drawing.Point(250,9)
$Labelconnected.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',13)

$Label1adminuser                          = New-Object system.Windows.Forms.Label
$Label1adminuser.text                     = "Enter an admin user."
$Label1adminuser.AutoSize                 = $true
$Label1adminuser.width                    = 25
$Label1adminuser.height                   = 10
$Label1adminuser.location                 = New-Object System.Drawing.Point(20,9)
$Label1adminuser.Font                    = New-Object System.Drawing.Font("Times New Roman",14,[System.Drawing.FontStyle]::Italic::Bold)

$Emailselectionview                       = New-Object system.Windows.Forms.ComboBox
$Emailselectionview.text                  = "Select mail to view permission status."
$Emailselectionview.width                 = 250
$Emailselectionview.height                = 20
$Emailselectionview.location              = New-Object System.Drawing.Point(20,70)
$Emailselectionview.Font                  = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$CheckPermission                         = New-Object system.Windows.Forms.Button
$CheckPermission.text                    = "Check Permissions"
$CheckPermission.width                   = 135
$CheckPermission.height                  = 30
$CheckPermission.location                = New-Object System.Drawing.Point(300,70)
$CheckPermission.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
$CheckPermission.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
$CheckPermission.BackColor                  = [System.Drawing.ColorTranslator]::FromHtml("#417505")

$DisplayUserNameandEmail                         = New-Object system.Windows.Forms.Label
$DisplayUserNameandEmail.text                    = "Click here to display all Username and Email"
$DisplayUserNameandEmail.AutoSize                = $true
$DisplayUserNameandEmail.width                   = 200
$DisplayUserNameandEmail.height                  = 10
$DisplayUserNameandEmail.location                = New-Object System.Drawing.Point(520,110)
$DisplayUserNameandEmail.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',12,[System.Drawing.FontStyle]([System.Drawing.FontStyle]::Underline))
$DisplayUserNameandEmail.ForeColor               = [System.Drawing.ColorTranslator]::FromHtml("#2634fc")

$DisplayUserNameandEmail_all                         = New-Object system.Windows.Forms.Button
$DisplayUserNameandEmail_all.text                    = "Display All Users"
$DisplayUserNameandEmail_all.width                   = 140
$DisplayUserNameandEmail_all.height                  = 20
$DisplayUserNameandEmail_all.location                = New-Object System.Drawing.Point(20,465)
$DisplayUserNameandEmail_all.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
$DisplayUserNameandEmail_all.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
$DisplayUserNameandEmail_all.BackColor                  = [System.Drawing.ColorTranslator]::FromHtml("#417505")

$Output_all_users = New-Object System.Windows.Forms.TextBox
$Output_all_users.Multiline = $true
$Output_all_users.ScrollBars = "Vertical"
$Output_all_users.Location = New-Object System.Drawing.Point(20, 490)
$Output_all_users.Size = New-Object System.Drawing.Size(810, 220)
$Output_all_users.Font = $font
$Output_all_users.Visible = $false

function drawingOutput_all_users{
$Output_all_users.Font                     = New-Object System.Drawing.Font('Lucida Console',20)
$Output_all_users.ForeColor                = [System.Drawing.ColorTranslator]::FromHtml("#f8e71c")
$Output_all_users.BackColor                = [System.Drawing.ColorTranslator]::FromHtml("#012456")
}
drawingOutput_all_users

#$Output                          = New-Object system.Windows.Forms.RichTextBox
$Output                          = New-Object system.Windows.Forms.TextBox
$Output.text                     = Goodmornig
$Output.multiline                = $true
$Output.ScrollBars               = "Vertical"
$Output.width                    = 810
$Output.height                   = 270
#$Output.Anchor                   = 'top,right,bottom,left'
$Output.location                 = New-Object System.Drawing.Point(20,190)
function drawingOutput{
$Output.Font                     = New-Object System.Drawing.Font('Lucida Console',20)
$Output.ForeColor                = [System.Drawing.ColorTranslator]::FromHtml("#f8e71c")
$Output.BackColor                = [System.Drawing.ColorTranslator]::FromHtml("#012456")
}
drawingOutput

$LabelFforselectemail                          = New-Object system.Windows.Forms.Label
$LabelFforselectemail.text                     = "Select the email that receives the permission."
$LabelFforselectemail.AutoSize                 = $true
$LabelFforselectemail.width                    = 25
$LabelFforselectemail.height                   = 10
$LabelFforselectemail.location                 = New-Object System.Drawing.Point(20,110)
$LabelFforselectemail.Font                     = New-Object System.Drawing.Font('Microsoft Sans Serif',12)

$Labelemailto                          = New-Object system.Windows.Forms.Label
$Labelemailto.text                     = "To:"
$Labelemailto.AutoSize                 = $true
$Labelemailto.width                    = 25
$Labelemailto.height                   = 10
$Labelemailto.location                 = New-Object System.Drawing.Point(20,135)
$Labelemailto.Font                     = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$Receiveemail                       = New-Object system.Windows.Forms.ComboBox
$Receiveemail.text                  = "Select the receiving email"
$Receiveemail.width                 = 210
$Receiveemail.height                = 20
$Receiveemail.location              = New-Object System.Drawing.Point(50,135)
$Receiveemail.Font                  = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$PermissionType                          = New-Object system.Windows.Forms.ComboBox
$PermissionType.text                     = "Select Access Rights."
$PermissionType.AutoSize                 = $true
$PermissionType.width                    = 160
$PermissionType.height                   = 10
$PermissionType.location                 = New-Object System.Drawing.Point(280,135)
$PermissionType.Font                     = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
@('Owner','Editor','Reviewer','None','NonEditingAuthor','Author','PublishingEditor','PublishingAuthor','Contributor','AvailabilityOnly','LimitedDetails') | ForEach-Object {[void] $PermissionType.Items.Add($_)}

$AddPermissions                          = New-Object system.Windows.Forms.Button
$AddPermissions.text                     = "Add"
$AddPermissions.AutoSize                 = $true
$AddPermissions.width                    = 75
$AddPermissions.height                   = 10
$AddPermissions.location                 = New-Object System.Drawing.Point(460,135)
$AddPermissions.Font                     = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
$AddPermissions.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
$AddPermissions.BackColor                  = [System.Drawing.ColorTranslator]::FromHtml("#417505")

$SetPermissions                          = New-Object system.Windows.Forms.Button
$SetPermissions.text                     = "Set"
$SetPermissions.AutoSize                 = $true
$SetPermissions.width                    = 75
$SetPermissions.height                   = 10
$SetPermissions.location                 = New-Object System.Drawing.Point(540,135)
$SetPermissions.Font                     = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
$SetPermissions.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
$SetPermissions.BackColor                  = [System.Drawing.ColorTranslator]::FromHtml("#417505")

$RemovePermissions                          = New-Object system.Windows.Forms.Button
$RemovePermissions.text                     = "Remove"
$RemovePermissions.AutoSize                 = $true
$RemovePermissions.width                    = 75
$RemovePermissions.height                   = 10
$RemovePermissions.location                 = New-Object System.Drawing.Point(620,135)
$RemovePermissions.Font                     = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
$RemovePermissions.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
$RemovePermissions.BackColor                  = [System.Drawing.ColorTranslator]::FromHtml("#417505")

$CheckBoxsendinfo                       = New-Object system.Windows.Forms.CheckBox
$CheckBoxsendinfo.text                  = "Mark V if you don't want to receive email"
$CheckBoxsendinfo.AutoSize              = $false
$CheckBoxsendinfo.width                 = 270
$CheckBoxsendinfo.height                = 20
$CheckBoxsendinfo.location              = New-Object System.Drawing.Point(20,160)
$CheckBoxsendinfo.Font                  = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

[System.Windows.Forms.ToolTip]$ToolTipMain = $null
$ToolTipMain = (New-Object -TypeName System.Windows.Forms.ToolTip)
Add-Member -InputObject $Tab1 -Name ToolTipMain -Value $ToolTipMain -MemberType NoteProperty

$helpbtn                         = New-Object system.Windows.Forms.Label
$helpbtn.text                    = "Help"
$helpbtn.AutoSize                = $true
$helpbtn.width                   = 25
$helpbtn.height                  = 10
$helpbtn.location                = New-Object System.Drawing.Point(800,20)
$helpbtn.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',12,[System.Drawing.FontStyle]([System.Drawing.FontStyle]::Underline))
$helpbtn.ForeColor               = [System.Drawing.ColorTranslator]::FromHtml("#2634fc")
$helpbtn_ToolTip = [System.String]'Help of explanation about individual permissions are available'
$ToolTipMain.SetToolTip($helpbtn, $helpbtn_ToolTip)

$Logpermissions                         = New-Object system.Windows.Forms.Label
$Logpermissions.text                    = "Log"
$Logpermissions.AutoSize                = $true
$Logpermissions.width                   = 25
$Logpermissions.height                  = 10
$Logpermissions.location                = New-Object System.Drawing.Point(800,40)
$Logpermissions.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',12,[System.Drawing.FontStyle]([System.Drawing.FontStyle]::Underline))
$Logpermissions.ForeColor               = [System.Drawing.ColorTranslator]::FromHtml("#2634fc")
$Logpermissions_ToolTip = [System.String]'logs of each permission granted'
$ToolTipMain.SetToolTip($Logpermissions, $Logpermissions_ToolTip)

$backgroundblack                         = New-Object system.Windows.Forms.Label
$backgroundblack.width                   = 20
$backgroundblack.height                  = 16
$backgroundblack.location                = New-Object System.Drawing.Point(2,2)
$backgroundblack.BackColor               = "black"
$backgroundblack.Add_Click({
$CalendarPermissions.backcolor = "black"
$Tab1.backcolor = "black"
$Tab1.ForeColor = "white"
})

$backgroundwhite                         = New-Object system.Windows.Forms.Label
$backgroundwhite.width                   = 20
$backgroundwhite.height                  = 16
$backgroundwhite.location                = New-Object System.Drawing.Point(24,2)
$backgroundwhite.BackColor               = "white"
$backgroundwhite.Add_Click({
$CalendarPermissions.backcolor = "white"
$Tab1.backcolor = "white"
$Tab1.ForeColor = "black"
})

$backgroundblue                         = New-Object system.Windows.Forms.Label
$backgroundblue.width                   = 20
$backgroundblue.height                  = 16
$backgroundblue.location                = New-Object System.Drawing.Point(46,2)
$backgroundblue.BackColor               = "#4fc3f7"
$backgroundblue.Add_Click({
$CalendarPermissions.backcolor = "#4fc3f7"
$Tab1.backcolor = "#4fc3f7"
$Tab1.ForeColor = "black"
})

$background                         = New-Object system.Windows.Forms.Label
$background.width                   = 81
$background.height                  = 20
$background.location                = New-Object System.Drawing.Point(785,90)
$background.BackColor               = "red"
$background.Add_Click({
$CalendarPermissions.backcolor = "#4fc3f7"
$Tab1.backcolor = "#4fc3f7"
$Tab1.ForeColor = "black"
})

$Exit                         = New-Object system.Windows.Forms.Button
$Exit.text                    = "Exit"
$Exit.width                   = 60
$Exit.height                  = 30
$Exit.location                = New-Object System.Drawing.Point(770,135)
$Exit.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',10)
$Exit.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat

$EngHeb                         = New-Object system.Windows.Forms.ComboBox
$EngHeb.text                    = "English"
$EngHeb.AutoSize                = $true
$EngHeb.width                   = 65
$EngHeb.height                  = 20
$EngHeb.location                = New-Object System.Drawing.Point(785,65)
$EngHeb.Font                    = New-Object System.Drawing.Font('Microsoft Sans Serif',9)
$hebrew = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 20 | select -last 1
@('English',$hebrew) | ForEach-Object {[void] $EngHeb.Items.Add($_)}

$Tab1.controls.AddRange(@($pleasewait,$Emailadmin,$Checkinternet,$Bottonconnect,$Labelconnected,$Label1adminuser,$Emailselectionview))
$Tab1.controls.AddRange(@($CheckPermission,$Output,$LabelFforselectemail,$Labelemailto,$AddPermissions,$DisplayUserNameandEmail,$DisplayUserNameandEmail_all,$Output_all_users,$SetPermissions))
$Tab1.controls.AddRange(@($PermissionType,$Receiveemail,$CheckBoxsendinfo,$removepermissions,$Logpermissions,$helpbtn,$Exit,$EngHeb,$background))
$background.controls.AddRange(@($backgroundblack,$backgroundwhite,$backgroundblue))
$currentTime = Get-Date -format "HH:mm:ss dd-MMM-yyyy"
Checkinternet

$ErrorProvider1 = New-Object System.Windows.Forms.ErrorProvider
$ErrorProvider2 = New-Object System.Windows.Forms.ErrorProvider
$ErrorProvider3 = New-Object System.Windows.Forms.ErrorProvider

$CalendarPermissions.AcceptButton = $Bottonconnect
$CalendarPermissions.CancelButton = $Exit

$Bottonconnect.Add_Click({$Output.ForeColor = "yellow";$pleasewait.text = "Please wait...";$pleasewait.backcolor = "green";connectadmin;$pleasewait.text = "";$pleasewait.BackColor = "Transparent" })
$CheckPermission.Add_Click({$Output.ForeColor = "yellow";$pleasewait.text = "Please wait...";$pleasewait.backcolor = "green"; Fun_checkPermissions;$pleasewait.text = "";$pleasewait.BackColor = "Transparent" })
$AddPermissions.Add_Click({$Output.ForeColor = "yellow";$pleasewait.text = "Please wait...";$pleasewait.backcolor = "green"; Add-Permissions;$pleasewait.text = "";$pleasewait.BackColor = "Transparent"  })
$SetPermissions.Add_Click({$Output.ForeColor = "yellow";$pleasewait.text = "Please wait...";$pleasewait.backcolor = "green"; Set-Permissions;$pleasewait.text = "";$pleasewait.BackColor = "Transparent"  })
$RemovePermissions.Add_Click({$Output.ForeColor = "yellow";$pleasewait.text = "Please wait...";$pleasewait.backcolor = "green";Remove-Permissions;$pleasewait.text = "";$pleasewait.BackColor = "Transparent"  })
$Emailadmin.Add_Leave({if($Emailadmin.Text -eq "" ){$Emailadmin.Text = "Please enter an admin email address"}})
$Emailadmin.Add_KeyDown({if(!($Emailadmin.Text -eq "")){$Emailadmin.Text = ""}})
$helpbtn.Add_Click({$Output.ForeColor = "yellow"; helpme })
$EngHeb.Add_SelectedIndexChanged({ if($EngHeb.text -eq "English"){languageEng}else{languageHeb}})
$EngHeb.Add_SelectedIndexChanged({ if($EngHeb.text -eq "English"){languageEng}else{languageHeb}})
$Logpermissions.Add_Click({ .\log.txt })
$DisplayUserNameandEmail.Add_Click({ Display_UserName_and_Email })
$DisplayUserNameandEmail_all.Add_Click({ Display_hide_All_Users })
$CheckBoxsendinfo.Add_CheckedChanged({ CheckBox_change_clolor })
$PermissionType.Add_Leave({ Test-Select-Access  })
$Emailselectionview.Add_Leave({ Test-Mailbox01  })
$Receiveemail.Add_Leave({ Test-Mailbox02  })
$Exit.Add_MouseHover({ $Exit.backcolor = "red" })
$Exit.Add_MouseLeave({ $Exit.backcolor = "" })

getalluser
$connectedtodomain = (Get-AcceptedDomain | Where{$_.Default -eq 'True'}).DomainName  | out-string
$Labelconnected.Text = "Connected to:$connectedtodomain"


function connectadmin {
        $useradmin = $Emailadmin.Text
        Connect-ExchangeOnline -UserPrincipalName $useradmin -ShowProgress $true
        $Bottonconnect.text  = "Connected"
        $connectedtoadmin = (Get-Mailbox -ResultSize Unlimited | Where-Object {$_.Name -eq "admin"} | Select-Object DisplayName).displayname
        $connectedtoEmailadmin = (Get-Mailbox -ResultSize Unlimited | Where-Object {$_.Name -eq "admin*"} | Select-Object PrimarySmtpAddress).PrimarySmtpAddress
        $connectedtodomain = (Get-AcceptedDomain | Where{$_.Default -eq 'True'}).DomainName  | out-string
        $Labelconnected.Text = "Connected to:$connectedtodomain"
        getalluser
        if([string]::IsNullOrEmpty($Emailadmin.Text)){
            $Emailadmin.Text = "$connectedtoEmailadmin"
            } 
}
function getalluser {
        $Emailselectionview.Items.Clear()
        $Receiveemail.Items.Clear()
        $allusersemail =  Get-Mailbox -ResultSize Unlimited | Select-Object PrimarySmtpAddress | Sort-Object PrimarySmtpAddress

    foreach ($useremail in $allusersemail) {
            $Emailselectionview.Items.Add("$($useremail.PrimarySmtpAddress)")
            $Receiveemail.Items.Add("$($useremail.PrimarySmtpAddress)")             
        }
}
function DisplayUserNameandEmail {
        $allusersemail =  Get-Mailbox -ResultSize Unlimited | Select-Object DisplayName,PrimarySmtpAddress | Sort-Object DisplayName | out-string
        $Output.Text = "$currentTime `r`n$allusersemail"
        $Output.Font = New-Object System.Drawing.Font('Lucida Console',12) 
}
function Display_hide_All_Users {
    if ($Output_all_users.Visible) {
        $Output_all_users.Visible = $false
        $DisplayUserNameandEmail_all.Text = "Display All Users"
        $txtLog.AppendText("Hid all users.rn")
        $CalendarPermissions.ClientSize                 = New-Object System.Drawing.Point(860,520)
        $FormTabControl.Size = "858,540"
    } else {
        $Output_all_users.Visible = $true
        $DisplayUserNameandEmail_all.Text = "Hide All Users"
        Display_UserName_and_Email_all
        $CalendarPermissions.ClientSize = New-Object System.Drawing.Point(860,760)
        $FormTabControl.Size = "858,750"

    }
    }
function DisplayUserNameandEmail_all {
        $allusersemail =  Get-Mailbox -ResultSize Unlimited | Select-Object DisplayName,PrimarySmtpAddress | Sort-Object DisplayName | out-string
        $Output_all_users.Text = "$allusersemail"
        $Output_all_users.Font = New-Object System.Drawing.Font('Lucida Console',12) 
}
function Display_UserName_and_Email_all {
        $Output_all_users.ForeColor = "yellow"
        $pleasewait.text = "Please wait...";$pleasewait.backcolor = "green"
        $getmail = Get-Mailbox -ResultSize Unlimited
        if($getmail){
            DisplayUserNameandEmail_all
            }else{
                $Output_all_users.Font = New-Object System.Drawing.Font('Lucida Console',14)
                $Output_all_users.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
                $Output_all_users.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
                $Output_all_users.Text = "$currentTime `r`n`r`nNot connected to Office 365, please connect first"
        }
$pleasewait.text = ""
$pleasewait.BackColor = "Transparent"  
}

function Fun_checkPermissions {
        drawingOutput

        if($Emailselectionview.text -eq "Select mail to view permission status."){
        $Output.Text = "$currentTime `r`n`r`nPlease select an email to check permissions"
        $Output.Font = New-Object System.Drawing.Font('Lucida Console',14)
        $Output.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
        $Output.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
        Test-Mailbox01
        }else{
            Test-Mailbox01
            $eml = ""
            $eml = $Emailselectionview.SelectedItem
            $namecalendar  = (Get-MailboxFolderStatistics -Identity $eml | Where-Object {$_.FolderType -eq "Calendar"}).name
            $statuscal = Get-MailboxFolderPermission -Identity $eml':'\$namecalendar | Select-Object User, AccessRights | Format-Table -AutoSize | out-string
            $Output.Text = "$currentTime `r`nThe users who have permission on $eml `r`n$statuscal"
            $Output.Font = New-Object System.Drawing.Font('Lucida Console',12)
        }
}

function Add-Permissions { 
        drawingOutput
    if($Receiveemail.text -eq "Select the receiving email" ){
            $Output.Text = "$currentTime `r`n`r`nPlease Select the receiving email"
            $Output.Font = New-Object System.Drawing.Font('Lucida Console',14)
            $Output.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
            $Output.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
            }else{
                Test-Select-Access
                Test-Mailbox02
                Test-Mailbox01
                $eml = $Emailselectionview.SelectedItem
                $emll = $Receiveemail.SelectedItem
                $pm = $PermissionType.text
                $namecalendar  = (Get-MailboxFolderStatistics -Identity $eml | Where-Object {$_.FolderType -eq "Calendar"}).name
                $Error.Clear()
                Add-MailboxFolderPermission -Identity $eml':'\$namecalendar -User $emll -AccessRights $pm 

                if($Error){
                    $Output.Font = New-Object System.Drawing.Font('Lucida Console',14)
                    $Output.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
                    $Output.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
                    $TypeMailboxeml = (Get-Mailbox -Identity  $eml | Select-Object RecipientTypeDetails).RecipientTypeDetails
                    $TypeMailboxemll = (Get-Mailbox -Identity $emll | Select-Object RecipientTypeDetails).RecipientTypeDetails
                    $Output.text = "$currentTime `r`n`r`nErrorMessage`r`n$Error`r`n`r`n***Check The Convert to regular/shared mailbox on email $emll. `r`n`r`nIf the email is SharedMailbox, Convert to regular/userMailbox.`r`nThe Email $eml is: $TypeMailboxeml`r`nThe Email $emll is: $TypeMailboxemll"
                    }else{
                        clear-host
                        $info = Get-MailboxFolderPermission -Identity $eml':'\$namecalendar | Select-Object User, AccessRights | out-string
                        $Output.text = "$currentTime `r`nThe users who have permission on $eml':'`r`n $info"
                        $Output.Font = New-Object System.Drawing.Font('Lucida Console',12)
                        $performancehistory = "[$currentTime] ---  $emll received $pm permission for $eml calendar"
                        checkboxV
                        $performancehistory >> .\log.txt
                        }
        }
}
function Set-Permissions {
            drawingOutput
            if($Receiveemail.text -eq "Select the receiving email" ){
            $Output.Text = "$currentTime `r`n`r`nPlease Select the receiving email"
            $Output.Font = New-Object System.Drawing.Font('Lucida Console',14)
            $Output.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
            $Output.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
            
            }else{
                Test-Select-Access
                Test-Mailbox01
                Test-Mailbox02
                $eml = $Emailselectionview.SelectedItem
                $emll = $Receiveemail.SelectedItem
                $pm = $PermissionType.text
                $namecalendar  = (Get-MailboxFolderStatistics -Identity $eml | Where-Object {$_.FolderType -eq "Calendar"}).name
                $Error.Clear()
                Set-MailboxFolderPermission -Identity $eml":\$namecalendar" -User $emll -AccessRights $pm
                if($Error){
                    $Output.Font = New-Object System.Drawing.Font('Lucida Console',14)
                    $Output.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
                    $Output.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
                    $TypeMailboxeml = (Get-Mailbox -Identity  $eml | Select-Object RecipientTypeDetails).RecipientTypeDetails
                    $TypeMailboxemll = (Get-Mailbox -Identity $emll | Select-Object RecipientTypeDetails).RecipientTypeDetails
                    $Output.text = "$currentTime `r`n`r`nErrorMessage`r`n$Error`r`n`r`n***Check The Convert to regular/shared mailbox on email $emll. `r`n`r`nIf the email is SharedMailbox, Convert to regular/userMailbox.`r`nThe Email $eml is: $TypeMailboxeml`r`nThe Email $emll is: $TypeMailboxemll"
                    }else{
                        clear-host
                        $info = Get-MailboxFolderPermission -Identity $eml':'\$namecalendar | Select-Object User, AccessRights | out-string
                        $Output.text = "$currentTime `r`nThe users who have permission on $eml':'`r`n $info"
                        $Output.Font                     = New-Object System.Drawing.Font('Lucida Console',12)
                        $performancehistory = "[$currentTime] ---  $emll received $pm permission for $eml calendar"
                        checkboxV
                        $performancehistory >> .\log.txt
                        }
              }
}
function Remove-Permissions {
        drawingOutput
        if($Receiveemail.text -eq "Select the receiving email" ){
        $Output.Text = "$currentTime `r`n`r`nPlease Select the receiving email"
        $Output.Font = New-Object System.Drawing.Font('Lucida Console',14)
        $Output.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
        $Output.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
        Test-Mailbox02
        }else{
            Test-Mailbox01
            Test-Mailbox02
            $eml = $Emailselectionview.SelectedItem
            $emll = $Receiveemail.SelectedItem
            $namecalendar  = (Get-MailboxFolderStatistics -Identity $eml | Where-Object {$_.FolderType -eq "Calendar"}).name
            Remove-MailboxFolderPermission -Identity $eml":\$namecalendar" -User $emll -Confirm:$false
            clear-host
            $info = Get-MailboxFolderPermission -Identity $eml':'\$namecalendar | Select-Object User, AccessRights | out-string
            $Output.text = "$currentTime `r`nThe users who have permission on $eml':'`r`n $info"
            $Output.Font = New-Object System.Drawing.Font('Lucida Console',12)
            $performancehistory = "[$currentTime] ---  $emll removed the permission for $eml calendar"
            checkboxV
            $performancehistory >> .\log.txt
        }
}

function checkboxV {
        $CheckBoxsendinfo.backcolor = ""
        if (!($CheckBoxsendinfo.Checked)) { 
            Sendemailperformancehistory
            }else{
                $CheckBoxsendinfo.backcolor = "yellow"
                $CheckBoxsendinfo.forecolor = "blue" 
           }
}
function Sendemailperformancehistory {
        $connectedtodomain = (Get-AcceptedDomain | Where{$_.Default -eq 'True'}).DomainName  | out-string
        Add-Type -AssemblyName "Microsoft.Office.Interop.Outlook" | Out-Null
        $outlook = New-Object -ComObject Outlook.Application
        $localmail = $outlook.CreateItem(0)
        $mymailbox = $outlook.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress
        $localmail.Subject = "Calendar permissions domain $connectedtodomain"
        $localmail.Body = "$performancehistory was made by $env:username"
        $localmail.To = "$mymailbox"
        $localmail.Send()
        [System.Runtime.Interopservices.Marshal]::ReleaseComObject($localmail) | Out-Null
        [System.Runtime.Interopservices.Marshal]::ReleaseComObject($outlook) | Out-Null
        Remove-Variable outlook, localmail -ErrorAction SilentlyContinue
}
function Helpme { 
    $helptext = "
The following individual permissions are available:

CreateItems: The user can create items in the specified folder.
CreateSubfolders: The user can create subfolders in the specified folder.
DeleteAllItems: The user can delete all items in the specified folder.
DeleteOwnedItems: The user can only delete items that they created from the specified folder.
EditAllItems: The user can edit all items in the specified folder.
EditOwnedItems: The user can only edit items that they created in the specified folder.
FolderContact: The user is the contact for the specified public folder.
FolderOwner: The user is the owner of the specified folder. The user can view the folder, move the folder, and create subfolders. The user can't read items, edit items, delete items, or create items.
FolderVisible: The user can view the specified folder, but can't read or edit items within the specified public folder.
ReadItems: The user can read items within the specified folder.

The roles that are available, along with the permissions that they assign, are described in the following list:

Author:CreateItems, DeleteOwnedItems, EditOwnedItems, FolderVisible, ReadItems
Contributor:CreateItems, FolderVisible
Editor:CreateItems, DeleteAllItems, DeleteOwnedItems, EditAllItems, EditOwnedItems, FolderVisible, ReadItems
None:FolderVisible
NonEditingAuthor:CreateItems, FolderVisible, ReadItems
Owner:CreateItems, CreateSubfolders, DeleteAllItems, DeleteOwnedItems, EditAllItems, EditOwnedItems, FolderContact, FolderOwner, FolderVisible, ReadItems
PublishingEditor:CreateItems, CreateSubfolders, DeleteAllItems, DeleteOwnedItems, EditAllItems, EditOwnedItems, FolderVisible, ReadItems
PublishingAuthor:CreateItems, CreateSubfolders, DeleteOwnedItems, EditOwnedItems, FolderVisible, ReadItems
Reviewer:FolderVisible, ReadItems

The following roles apply specifically to calendar folders:

AvailabilityOnly: View only availability data
LimitedDetails: View availability data with subject and location"

$helptextheb1 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 1 | select -First 1
$helptextheb2 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 2 | select -First 1
$helptextheb3 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 3 | select -First 1
$helptextheb4 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 4 | select -First 1
$helptextheb5 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 5 | select -First 1
$helptextheb6 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 6 | select -First 1
$helptextheb7 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 7 | select -First 1
$helptextheb8 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 8 | select -First 1
$helptextheb9 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 9 | select -First 1
$helptextheb10 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 10 | select -First 1
$helptextheb11 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 11 | select -First 1
$helptextheb12 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 12 | select -First 1
$helptextheb13 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 13 | select -First 1
$helptextheb14 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 14 | select -First 1
$helptextheb15 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 15 | select -First 1
$helptextheb16 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 16 | select -First 1
$helptextheb17 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 17 | select -First 1
$helptextheb18 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 18 | select -First 1
$helptextheb19 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 19 | select -First 1
$helptextheb20 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 20 | select -First 1
$helptextheb21 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 21 | select -First 1
$helptextheb22 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 22 | select -First 1
$helptextheb23 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 23 | select -First 1
$helptextheb24 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 24 | select -First 1
$helptextheb25 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 25 | select -First 1
$helptextheb26 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 26 | select -First 1
$helptextheb27 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 27 | select -First 1
$helptextheb28 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 28 | select -First 1
$helptextheb29 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 29 | select -First 1
$helptextheb30 = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -last 30 | select -First 1
$helptextheb = "
$helptextheb30`r`n$helptextheb29`r`n$helptextheb28`r`n$helptextheb27`r`n$helptextheb26`r`n$helptextheb25`r`n$helptextheb24`r`n$helptextheb23`r`n$helptextheb22`r`n$helptextheb21`r`n$helptextheb20
$helptextheb19`r`n$helptextheb18`r`n$helptextheb17`r`n$helptextheb16`r`n$helptextheb15`r`n$helptextheb4`r`n$helptextheb3`r`n$helptextheb12`r`n$helptextheb11`r`n$helptextheb10
$helptextheb9`r`n$helptextheb8`r`n$helptextheb7`r`n$helptextheb6`r`n$helptextheb5`r`n$helptextheb4`r`n$helptextheb3`r`n$helptextheb2`r`n$helptextheb1"


if($helpbtn.text -eq "Help"){    
$Output.text = "$currentTime`r`n$helptext"
}else{
$Output.text = "$currentTime`r`n$helptextheb"
}
$Output.Font                     = New-Object System.Drawing.Font('Lucida Console',12)
}
function languageEng {
$CalendarPermissions.text                         = "Office 365 calendar permissions                                           Hello, $env:username! "
$Emailadmin.Text                                  = "Please enter an admin email address"
$Bottonconnect.text                               = "Connect"
$Labelconnected.Text                              = "Connected to:"
$Label1adminuser.text                             = "Enter an admin user."
$Emailselectionview.text                          = "Select mail to view permission status."
$CheckPermission.text                             = "Check Permissions"
$DisplayUserNameandEmail.text                     = "Click here to display all Username and Email"
$Output.text                                      = Goodmornig
$LabelFforselectemail.text                        = "Select the email that receives the permission."
$Receiveemail.text                                = "Select the receiving email"
$PermissionType.text                              = "Select Access Rights."
$AddPermissions.text                              = "Add"
$SetPermissions.text                              = "Set"
$RemovePermissions.text                           = "Remove"
$CheckBoxsendinfo.text                            = "Mark V if you don't want to receive email"
$helpbtn.text                                     = "Help"
$Logpermissions.text                              = "Log"
$Exit.text                                        = "Exit"
$Checkinternet.text                               = "No internet connection available."
$Checkinternet.text                               = "Internet connection is available."

}
function languageHeb {
$Emailadmin.Text                                  = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 1
$Bottonconnect.text                               = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 2 | select -last 1
$Labelconnected.Text                              = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 3 | select -last 1
$Label1adminuser.text                             = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 4 | select -last 1
$Emailselectionview.text                          = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 5 | select -last 1
$CheckPermission.text                             = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 6 | select -last 1
$DisplayUserNameandEmail.text                     = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 7 | select -last 1
$Output.text                                      = GoodmornigHeb
$LabelFforselectemail.text                        = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 8 | select -last 1
$Receiveemail.text                                = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 9 | select -last 1
$PermissionType.text                              = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 10 | select -last 1
$AddPermissions.text                              = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 11 | select -last 1
$SetPermissions.text                              = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 12 | select -last 1
$RemovePermissions.text                           = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 13 | select -last 1
$CheckBoxsendinfo.text                            = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 14 | select -last 1
$helpbtn.text                                     = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 15 | select -last 1
$Logpermissions.text                              = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 16 | select -last 1
$Exit.text                                        = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 17 | select -last 1
$Checkinternet.text                               = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 18 | select -last 1
$Checkinternet.text                               = Get-Content -Path .\languageHeb.txt -Encoding UTF8 | select -First 19 | select -last 1
}


function check_if_user_admin {
        $currentPid = [System.Security.Principal.WindowsIdentity]::GetCurrent()
        $principal = new-object System.Security.Principal.WindowsPrincipal($currentPid)
        $adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator
        if ($principal.IsInRole($adminRole)) {
            $Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Admin)"
            clear-host
            }else{
                Write-Host "===========================================" -Foregroundcolor DarkRed
                Write-Host "-- Scripts must be run as Administrator ---" -Foregroundcolor DarkRed
                Write-Host "-- Right-Click Start -> Terminal(Admin) ---" -Foregroundcolor DarkRed
                Write-Host "===========================================" -Foregroundcolor DarkRed
                [System.Windows.MessageBox]::Show("Please run this script as Administrator!", "Error", [System.Windows.MessageBoxButton]::OK, [System.Windows.MessageBoxImage]::Error)
                break

        }
}
function check_Module_ExchangeOnlineManagement {
        $Module = (Get-Module ExchangeOnlineManagement -ListAvailable) | where {$_.Version.major -ge 3}
        if($Module.count -eq 0){
            Write-Host `n`nExchange Online PowerShell module is not available -ForegroundColor yellow
            $Confirm= Read-Host Are you sure you want to install module? [Y] Yes [N] No
            if($Confirm -match "[yY]"){
                cls
                Write-host "Installing Exchange Online PowerShell module . . ."
                Install-Module ExchangeOnlineManagement -Repository PSGallery -AllowClobber -Force
                Import-Module ExchangeOnlineManagement
                }else{
                    cls    
                    Write-Host `n`nEXO module is required to connect Exchange Online. Please install module using Install-Module ExchangeOnlineManagement cmdlet. -ForegroundColor red `n`n
                    Exit
               }
        }
}
function Goodmornig {
        $hour = Get-Date -Format "HH"
        If ($Hour -lt 12) {"Good Morning  $env:username!"}  -Encoding UTF8
        ElseIf ($Hour -gt 17) {"Good Eventing $env:username!"}
        Else {"Good Afternoon $env:username!"}
}
function Checkinternet {
        $server = ""
        $server = "8.8.8.8"
        $Checkinternet.frontcolor= "white"
        if (Test-Connection -ComputerName $server -Count 1 -Quiet) {
            $Checkinternet.text = "Internet connection is available."
            $Checkinternet.backcolor= "green"
            } else {
                $Checkinternet.text = "No internet connection available."
                $Checkinternet.backcolor= "red"
       }     
}

function Test-Select-Access {
        try {
            $mailbox01 = $PermissionType.text -eq "Select Access Rights."
            } catch { }
        if ($mailbox01) {
            $ErrorProvider1.SetError($PermissionType, 'Select the relevant permission')
            } else { 
                $ErrorProvider1.Clear()
    }
}
function Test-Mailbox01 {
        try {
            $mailbox01 = Get-mailbox -Identity $Emailselectionview.Text
            } catch { }
        if ($mailbox01) {
            $ErrorProvider2.Clear()
            } else { 
                $ErrorProvider2.SetError($Emailselectionview, 'Select the email from the list')
    }
}
function Test-Mailbox02 {
        try {
            $mailbox01 = Get-mailbox -Identity $Receiveemail.Text
            } catch { }
        
        if ($mailbox01) {
            $ErrorProvider3.Clear()
            } else { 
                $ErrorProvider3.SetError($Receiveemail, 'Select the email from the list')
      } 
}

function Display_UserName_and_Email {
        $Output.ForeColor = "yellow"
        $pleasewait.text = "Please wait...";$pleasewait.backcolor = "green"
        $getmail = Get-Mailbox -ResultSize Unlimited
        if($getmail){
            DisplayUserNameandEmail
            }else{
                $Output.Font = New-Object System.Drawing.Font('Lucida Console',14)
                $Output.ForeColor = [System.Drawing.ColorTranslator]::FromHtml("Red")
                $Output.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#012456")
                $Output.Text = "$currentTime `r`n`r`nNot connected to Office 365, please connect first"
        }
$pleasewait.text = ""
$pleasewait.BackColor = "Transparent"  
}
function CheckBox_change_clolor{
        $CheckBoxsendinfo.backcolor = ""
        if ($CheckBoxsendinfo.Checked) { 
            $CheckBoxsendinfo.backcolor = "yellow"
            $CheckBoxsendinfo.forecolor = "blue" 
            }else{ 
                $CheckBoxsendinfo.backcolor = ""
                $CheckBoxsendinfo.forecolor = ""
            }
}

[void]$CalendarPermissions.ShowDialog()